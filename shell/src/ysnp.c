/*
Copyright (c) 2018 Kollins G. Lima
 
This file is part of YSNP.
 
YSNP is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published     
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
YSNP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with YSNP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ysnp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <posixeg/tparse.h>

#define PROMPT_SYMBOL ">"

int exit_flag = 0;

int main(int argc, const char *argv[])
{
    /*int i,j;*/
    int pid, return_pid, child_status;

    buffer_t *command_line;
    pipeline_t *pipeline;

    command_line = new_command_line();
    pipeline = new_pipeline();

    while (!exit_flag) {
        printf("%s ", PROMPT_SYMBOL);
        fflush(stdout);

        read_command_line(command_line);

        if(!parse_command_line(command_line, pipeline)){
            /*
            printf("Command: ");
            for (i = 0; pipeline->command[i][0]; i++) {     //Find commands
                for (j = 0; pipeline->command[i][j]; j++) { //Find arguments
                    printf("%s ", pipeline->command[i][j]);
                }
            }
            printf("\n");
            */

            if(check_internal_command(pipeline)){

                /*Not an internal command*/

                pid = fork();
            
                if (pid == 0) {
                    /* Child's code */
                    if(execvp(pipeline->command[0][0], pipeline->command[0]) < 0){
                        printf("Fail to execute command\n");
                        exit(EXIT_FAILURE);
                    }
                } else {
                    /* Father's code */
                    do {
                        return_pid = wait(&child_status);
                    } while(return_pid != pid);
                }
            }

        }

    }

    release_command_line(command_line);
    release_pipeline(pipeline);

    return EXIT_SUCCESS;
}
