#ifndef YSNP_H
#define YSNP_H

#include <posixeg/tparse.h>

#define NOT_INTERNAL_COMMAND 1
#define INTERNAL_COMMAND 0

#define EXIT_COMMAND "exit"

int check_internal_command(pipeline_t *pipeline);

#endif 
