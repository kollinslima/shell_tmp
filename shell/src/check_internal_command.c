/*
Copyright (c) 2018 Kollins G. Lima
 
This file is part of YSNP.
 
YSNP is free software: you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published     
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
YSNP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with YSNP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ysnp.h>
#include <string.h>
#include <posixeg/tparse.h>

/*External flag defined in ysnp.c*/
extern int exit_flag;

/*
 * Check for an internal command and execute it
 * 
 * Input: 
 * -pipeline containing input command
 *
 * Return: 
 * -INTERNAL_COMMAND -> if an internal command
 *  was found (and executed)
 * -NOT_INTERNAL_COMMAND -> if an internal command
 *  was not found. 
 */
int check_internal_command(pipeline_t *pipeline){

    /*Check exit command*/
    if (strcmp(pipeline->command[0][0], EXIT_COMMAND) == 0){
        /*Set global exit_flag*/
        exit_flag = 1;
        return INTERNAL_COMMAND;
    }

    return NOT_INTERNAL_COMMAND;
}
