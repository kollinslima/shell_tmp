# YSNP - You Shell Not Pass

A POSIX shell, containing a subset of POSIX utilities. 
YSNP depends on posixeg library, which can be found in https://gitlab.com/monaco/posixeg.git

For copyright and licensing information see files AUTHORS and COPYING.

YSNP's source code may be obtained from <URL>

Installation and execution are covered in file INSTALL.
